<?php

return [
    'id' => 'Id',
    'dishes' => 'Dishes',
    'name' =>   'Name',
    'image' => 'Image',
    'price' => 'Price',
    'yes' => 'Yes',
    'no' => 'No',
];
