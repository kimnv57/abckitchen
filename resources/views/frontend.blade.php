<!DOCTYPE html>
<html lang="en">
     <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>@section('title') Laravel 5 Sample Site @show</title>
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/bootstrap.min.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/mystyle.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/dish.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/footer.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/news.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/contacts.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/metro-bootstrap.css')}}">
     <link href="{{URL::to('css/iconFont.css')}}" rel="stylesheet">
     <script src="{{ URL::to('/js/jquery-1.9.1.min.js') }}"></script>
     <script language="JavaScript" src="{{ URL::to('/js/calendar_us.js') }}"></script>
      <link rel="stylesheet" href="{{URL::to('css/calendar.css')}}">


     <!-- <link href="{{ asset('/css/all.css') }}" rel="stylesheet"> -->
     @yield('styles')
     

     </head>
<body>
  @include('partials.normal.nav')
  <div>
    @yield('content')
    
  </div>
  </body>
  <div class="my_footer">
      @include('partials.footer')
      @include('partials.footer-design')
  </div>
<script src="{{ URL::to('/js/jquery.min.js') }}"></script>
<script src="{{ URL::to('/js/bootstrap.min.js') }}"></script>
<script type="text/javascript"  src="{{URL::to('js/calendar.js')}}"></script>
 <script type="text/javascript"  src="{{URL::to('js/jquery.widget.min.js')}}"></script>
 <script type="text/javascript"  src="{{URL::to('js/metro.min.js')}}"></script>
 <script type="text/javascript"  src="{{URL::to('js/metro-global.js')}}"></script>
 <script type="text/javascript"  src="{{URL::to('js/metro-locale.js')}}"></script>
 <script type="text/javascript"  src="{{URL::to('js/metro-calendar.js')}}"></script>

<script>
$(function(){
    $('.images1').hover(function(){
        $('.cs-carousel-body1').css('bottom','0px');
    },function(){
        $('.cs-carousel-body1').css('bottom', '-136px');
    });
    $('.images2').hover(function(){
        $('.cs-carousel-body2').css('bottom','0px');
    },function(){
        $('.cs-carousel-body2').css('bottom', '-136px');
    });
    $('.images3').hover(function(){
        $('.cs-carousel-body3').css('bottom','0px');
    },function(){
        $('.cs-carousel-body3').css('bottom', '-136px');
    });
    $('.previous').click(function(){
        $('.myrowfood > .active').next('div').find('.food').trigger('click');
    });

    $('.next').click(function(){
        $('.myrowfood > .active').prev('div').find('food').trigger('click');
    });
});
            function myFunction() {
                var x = document.createElement("INPUT");
                x.setAttribute("type", "date");
                x.setAttribute("value", "2014-02-09");
                document.body.appendChild(x);
            }
</script>
@yield('scripts')

</html>