<!DOCTYPE html>
<html lang="en">
     <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>@section('title') Abc kitchen @show</title>
     <link rel="icon" href="images/favicon.ico">
     <link rel="shortcut icon" href="images/favicon.ico" />
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/bootstrap.min.css')}}">
     <link rel="stylesheet" type="text/css" href="{{URL::to('css/backendstyle.css')}}">
     <script src="{{ URL::to('/js/jquery-1.9.1.min.js') }}"></script>
     <!-- <link href="{{ asset('/css/all.css') }}" rel="stylesheet"> -->
     @yield('styles')
     </head>
<body>
  @include('partials.dashboard.nav')
  <div class="container" id="body">
    @yield('content')
    @include('partials.footer')
  </div>
  @include('partials.footer-design')
<script src="{{ URL::to('/js/jquery.min.js') }}"></script>
<script src="{{ URL::to('/js/bootstrap.min.js') }}"></script>
@yield('scripts')
</body>
</html>