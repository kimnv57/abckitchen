@extends('backend')

{{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop
@section('styles')
<link rel="stylesheet" type="text/css" href="{{URL::to('/css/jquery.datatables.css')}}">
<link href="{{ URL::to('/css/colorbox.css') }}" rel="stylesheet" type="text/css">
@stop
{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>
            {{{ Lang::get("admin/users.users") }}}
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{{{ URL::to('admin/users/create') }}}"
                       class="btn btn-sm  btn-primary iframe"><span
                                class="glyphicon glyphicon-plus-sign"></span> {{
					Lang::get("admin/modal.new") }}</a>
                </div>
            </div>
        </h3>
    </div>
<div class="row">
    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th >{{{ Lang::get("admin/users.name") }}}</th>
            <th >{{{ Lang::get("admin/users.email") }}}</th>
            <th >Admin</th>
            <th >Accountant</th>
            <th >{{{ Lang::get("admin/admin.created_at") }}}</th>
            <th>{{{ Lang::get("admin/admin.action") }}}</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    </div>
@stop

{{-- Scripts --}}
@section('scripts')
    @parent

    <script type="text/javascript" src="{{ URL::to('/js/jquery.datatables.js') }}"></script>
    <script src="{{asset('assets/admin/js/bootstrap-dataTables-paging.js')}}"></script>
    <script src="{{{ asset('assets/admin/js/jquery.colorbox.js') }}}"></script>
    <script type="text/javascript">
        var oTable;
        $(document).ready(function () {
            // $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
            oTable = $('#table').dataTable({
               "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                "sPaginationType": "bootstrap",

                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "http://localhost:86/laravel/Laravel-5-Bootstrap-3-Starter-Site/public/users/data",
                "fnDrawCallback": function (oSettings) {
                    $(".iframe").colorbox({
                        iframe: true,
                        width: "80%",
                        height: "80%",
                        onClosed: function () {
                            window.location.reload();
                        }
                    });
                }
            });
        });
    </script>
@stop
