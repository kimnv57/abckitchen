@extends('admin/model')
@section('content')
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">{{{
			Lang::get('admin/modal.general') }}}</a></li>
</ul>
<form class="form-horizontal" method="post"
	action="@if (isset($dish)){{ URL::to('admin/dishes/' . $dish->id . '/edit') }}@endif"
	autocomplete="off">
	<div class="tab-content">
		<div class="tab-pane active" id="tab-general">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="name">{{
						Lang::get('admin/dishes.name') }}</label>
					<div class="col-md-10">
						<input class="form-control" tabindex="1"
							placeholder="{{ Lang::get('admin/dishes.name') }}" type="text"
							name="name" id="name"
							value="{{{ Input::old('name', isset($dish) ? $dish->name : null) }}}">
					</div>
				</div>
			</div>
            @if(!isset($dish))
            <div class="col-md-12">
                <div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="username">{{
						Lang::get('admin/dishes.username') }}</label>
                    <div class="col-md-10">
                        <input class="form-control" type="username" tabindex="4"
                               placeholder="{{ Lang::get('admin/dishes.username') }}" name="username"
                               id="username"
                               value="{{{ Input::old('username', isset($dish) ? $dish->username : null) }}}" />
                        {!! $errors->first('username', '<label class="control-label"
                                                            for="username">:message</label>')!!}
                    </div>
                </div>
            </div>
			<div class="col-md-12">
				<div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="email">{{
						Lang::get('admin/dishes.email') }}</label>
					<div class="col-md-10">
						<input class="form-control" type="email" tabindex="4"
							placeholder="{{ Lang::get('admin/dishes.email') }}" name="email"
							id="email"
							value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
						{!! $errors->first('email', '<label class="control-label"
							for="email">:message</label>')!!}
					</div>
				</div>
			</div>
			@endif
			<div class="col-md-12">
				<div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="password">{{
						Lang::get('admin/dishes.password') }}</label>
					<div class="col-md-10">
						<input class="form-control" tabindex="5"
							placeholder="{{ Lang::get('admin/dishes.password') }}"
							type="password" name="password" id="password" value="" />
						{!!$errors->first('password', '<label class="control-label"
							for="password">:message</label>')!!}
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="password_confirmation">{{
						Lang::get('admin/dishes.password_confirmation') }}</label>
					<div class="col-md-10">
						<input class="form-control" type="password" tabindex="6"
							placeholder="{{ Lang::get('admin/dishes.password_confirmation') }}"
							name="password_confirmation" id="password_confirmation" value="" />
						{!!$errors->first('password_confirmation', '<label
							class="control-label" for="password_confirmation">:message</label>')!!}
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<br>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="roles">{{
						Lang::get('admin/dishes.roles') }}</label>
					<div class="col-md-6">
						<select name="roles[]" id="roles" multiple style="width: 100%;">
							@foreach ($roles as $role)
							<option value="{{{ $role->id }}}" {{{ ( array_search($role->id,
								$selectedRoles) !== false && array_search($role->id,
								$selectedRoles) >= 0 ? ' selected="selected"' : '') }}}>{{{
								$role->name }}}</option> @endforeach
						</select> <span class="help-block"> {{
							Lang::get('admin/dishes.roles_info') }} </span>
					</div>
				</div>
			</div>e
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<button type="reset" class="btn btn-sm btn-warning close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> {{
				Lang::get("admin/modal.cancel") }}
			</button>
			<button type="reset" class="btn btn-sm btn-default">
				<span class="glyphicon glyphicon-remove-circle"></span> {{
				Lang::get("admin/modal.reset") }}
			</button>
			<button type="submit" class="btn btn-sm btn-success">
				<span class="glyphicon glyphicon-ok-circle"></span> 
				    @if	(isset($user))
				        {{ Lang::get("admin/modal.edit") }}
				    @else
				        {{Lang::get("admin/modal.create") }} 
				    @endif
			</button>
		</div>
	</div>
</form>
@stop
