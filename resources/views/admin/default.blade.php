@extends('backend')

{{-- Web site Title --}}
@section('title')
    Administration :: @parent
@endsection

{{-- Styles --}}
@section('styles')
    @parent

@endsection

{{-- Sidebar --}}
@section('content')
@endsection

{{-- Scripts --}}
@section('scripts')
    @parent
    {{-- Default admin scripts--}}


@endsection
