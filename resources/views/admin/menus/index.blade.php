@extends('backend')

{{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>
            Thực đơn
            <div class="pull-right">
                <div class="pull-right">
                    <a href="{{{ URL::to('admin/users/create') }}}"
                       class="btn btn-sm  btn-primary iframe"><span
                                class="glyphicon glyphicon-plus-sign"></span> {{
					Lang::get("admin/modal.new") }}</a>
                </div>
            </div>
        </h3>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="col-md-3">{{{ Lang::get("admin/users.name") }}}</th>
            <th class="col-md-3">{{{ Lang::get("admin/users.email") }}}</th>
            <th class="col-md-3">{{{ Lang::get("admin/admin.created_at") }}}</th>
            <th class="col-md-3">{{{ Lang::get("admin/admin.action") }}}</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
@stop

{{-- Scripts --}}
@section('scripts')
    @parent
@stop
