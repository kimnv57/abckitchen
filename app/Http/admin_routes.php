<?php
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
	Route::pattern('id', '[0-9]+');
    Route::pattern('id2', '[0-9]+');

    #Admin Dashboard
    Route::get('dashboard', 'Admin\DashboardController@index');

    #Users
    Route::get('users/', 'Admin\UserController@index');
    Route::get('users/create', 'Admin\UserController@getCreate');
    Route::post('users/create', 'Admin\UserController@postCreate');
    Route::get('users/{id}/edit', 'Admin\UserController@getEdit');
    Route::post('users/{id}/edit', 'Admin\UserController@postEdit');
    Route::get('users/{id}/delete', 'Admin\UserController@getDelete');
    Route::post('users/{id}/delete', 'Admin\UserController@postDelete');
    Route::get('users/data', 'Admin\UserController@data');
    #Dishes
    Route::get('dishes/', 'Admin\DishController@index');
    Route::get('dishes/create', 'Admin\DishController@getCreate');
    Route::post('dishes/create', 'Admin\DishController@postCreate');
    Route::get('dishes/{id}/edit', 'Admin\DishController@getEdit');
    Route::post('dishes/{id}/edit', 'Admin\DishController@postEdit');
    Route::get('dishes/{id}/delete', 'Admin\DishController@getDelete');
    Route::post('dishes/{id}/delete', 'Admin\DishController@postDelete');
    Route::get('dishes/data', 'Admin\DishController@data');
    #menus
    Route::get('menus/', 'Admin\MenuController@index');
    Route::get('menus/create', 'Admin\UserController@getCreate');
    Route::post('menus/create', 'Admin\UserController@postCreate');
    Route::get('menus/{id}/edit', 'Admin\UserController@getEdit');
    Route::post('menus/{id}/edit', 'Admin\UserController@postEdit');
    Route::get('menus/{id}/delete', 'Admin\UserController@getDelete');
    Route::post('menus/{id}/delete', 'Admin\UserController@postDelete');
    Route::get('menus/{eat_time}/data', 'Admin\MenuController@getmenu');
    #languages
    Route::get('languages/', 'Admin\LanguageController@index');
});