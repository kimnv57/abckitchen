<?php namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\User;
use App\Dish;

class DashboardController extends AdminController {
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$title = "Dashboard";
		$users = User::count();
		$dishes = Dish::count();
		return view('admin.dashboard.index',compact('title','users','dishes'));
		// return view('admin.dashboard.index');
	}

}