<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Dish;
use App\Menu;
use App\AssignedDishes;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserEditRequest;
use App\Http\Requests\Admin\DeleteRequest;


class MenuController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.menus.index');
    }

    public function getmenu($date) {
        $menu = Menu::where('eat_time','LIKE','%'.$date.'%')->lists('id');
        if(!empty($menu)) {
            $dishes = AssignedDishes::join('dishes','dishes.id','=','dishes_menus.dish_id')->where('menu_id',"=",$menu)->get();
            return $dishes;
        }
        else
            return null;

    }

}
