<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use App\Dish;
use App\Http\Requests\Admin\DishRequest;
use App\Http\Requests\Admin\DishEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;


class DishController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.dishes.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return view('admin.dishes.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(UserRequest $request) {

        $user = new Dish ();
        $user -> name = $request->name;
        $user -> username = $request->username;
        $user -> email = $request->email;
        $user -> password = bcrypt($request->password);
        $user -> save();
        foreach($request->roles as $item)
        {
            $role = new AssignedRoles();
            $role -> role_id = $item;
            $role -> user_id = $user -> id;
            $role -> save();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {

        $user = Dish::find($id);
        $roles = Role::all();
        $selectedRoles = AssignedRoles::where('user_id','=',$user->id)->lists('role_id');

        return view('admin.users.create_edit', compact('user', 'roles', 'selectedRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(UserEditRequest $request, $id) {

        $user = User::find($id);
        $user -> name = $request->name;

        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $user -> password = bcrypt($password);
            }
        }
        $user -> save();
        AssignedRoles::where('user_id','=',$user->id)->delete();
        foreach($request->roles as $item)
        {
            $role = new AssignedRoles;
            $role -> role_id = $item;
            $role -> user_id = $user -> id;
            $role -> save();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {
        $user = Dish::find($id);
        // Show the page
        return view('admin.users.delete', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$id)
    {
        $user= Dish::find($id);
        $user->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
       
    $dishes = Dish::select(array('dishes.id','dishes.name','dishes.image', 'dishes.price', 'dishes.created_at'))->orderBy('dishes.name', 'ASC');
        //$users = User::select(array('users.id','users.name','users.email', 'usedishesrs.created_at'))->orderBy('users.email', 'ASC');

        return Datatables::of($dishes)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/dishes/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/dishes/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->edit_column('image', '{!! ($image!="")? "<img style=\"max-width: 100px; max-height: 70px;\" src=\"../appfiles/dishimages/$image\">":""; !!}')
            ->make(true);
    }
   
}
