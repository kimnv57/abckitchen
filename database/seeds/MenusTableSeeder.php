<?php

use App\Menu;
use App\AssignedDishes;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder {

    public function run()
    {
        DB::table('menus')->delete();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-05-02');
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-05-03');
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-05-04');
        $menu->save();

        $menu = new Menu;
        $menu->eat_time = new DateTime('2015-05-05');
        $menu->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 1;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 2;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 1;
        $assigneddish->dish_id = 3;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 3;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 4;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 5;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 2;
        $assigneddish->dish_id = 6;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 3;
        $assigneddish->dish_id = 4;
        $assigneddish->save();

        $assigneddish = new AssignedDishes;
        $assigneddish->menu_id = 3;
        $assigneddish->dish_id = 1;
        $assigneddish->save();



    }

}