<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy

class UsersTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();
		\App\User::create([
			'name' => 'Admin User',
			'username' => 'admin_user',
			'email' => 'admin@gmail.com',
			'password' => bcrypt('12345'),
		]);

		\App\User::create([
			'name' => 'Test User',
			'username' => 'normal_user',
			'email' => 'accountant@gmail.com',
			'password' => bcrypt('12345'),
		]);

	}

}
