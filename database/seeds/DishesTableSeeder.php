<?php

use App\Dish;
use Illuminate\Database\Seeder;

class DishesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('dishes')->delete();

        $dish = new Dish;
        $dish->name = 'canh';
        $dish->image = '#';
        $dish->description = 'description';
        $dish->price = '10000';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cà ri';
        $dish->image = '#';
        $dish->description = 'description';
        $dish->price = '20000';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cá';
        $dish->image = '#';
        $dish->description = 'description';
        $dish->price = '15000';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Cơm';
        $dish->image = '#';
        $dish->description = 'description';
        $dish->price = '10000';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt';
        $dish->image = '#';
        $dish->description = 'description';
        $dish->price = '15000';
        $dish->save();

        $dish = new Dish;
        $dish->name = 'Thịt rán';
        $dish->image = '#';
        $dish->description = 'description';
        $dish->price = 18000;
        $dish->save();
    }

}
